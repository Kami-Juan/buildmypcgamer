import AgregarFuentePoder from '../components/fuentepoder/AgregarFuentePoder.vue';

export default [
  {
    path: '/fuentepoder',
    name: 'fuentepoderagregar',
    component: AgregarFuentePoder
  }
];
