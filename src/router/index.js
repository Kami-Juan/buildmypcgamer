import Vue from 'vue';
import Router from 'vue-router';

import Main from '../components/Main';
import Error from '../components/Error';
import PanelControl from '../components/Panel/Panel';
import FuentePoder from './fuentedepoder';
import Usuario from './usuario';


Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'main',
      component: Main
    },
    {
      path: '*',
      component:Error
    },
    {
      path: '/paneldecontrol',
      name: 'panelcontrol',
      component: PanelControl
    },
    ...FuentePoder,
    ...Usuario
  ]
})
